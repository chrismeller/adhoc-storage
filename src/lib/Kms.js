'use strict';

const AWS = require('aws-sdk');
const config = require('config');
const Promise = require('bluebird');

class Kms {
	constructor(logger) {
		const accessKey = config.get('aws.access_key');
		const secretKey = config.get('aws.secret_key');
		const region = config.get('aws.region');

		this.keyId = config.get('aws.kms.key_id');

		this.kms = new AWS.KMS({ accessKeyId: accessKey, secretAccessKey: secretKey, region: region });

		this.logger = logger;

		Promise.promisifyAll(this.kms);
	}

	async encrypt(plaintext) {
		try {
			const result = await this.kms.encryptAsync({
				Plaintext: Buffer.from(plaintext, 'utf8'),
				KeyId: this.keyId,
			});

			return result.CiphertextBlob;
		}
		catch (e) {
			this.logger.error('Unable to encrypt plaintext with KMS!', { error: e });

			throw e;
		}
	}

	async decrypt(ciphertext) {
		try {
			const result = await this.kms.decryptAsync({
				CiphertextBlob: Buffer.from(ciphertext, 'base64'),
			});

			return result.Plaintext;
		}
		catch (e) {
			this.logger.error('Unable to decrypt ciphertext with KMS!', { error: e });

			throw e;
		}
	}
}

module.exports = Kms;