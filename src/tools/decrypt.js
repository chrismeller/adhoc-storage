'use strict';

const KMS = require('../lib/Kms');
const pino = require('pino')({ level: 'debug' });

(async () => {
	const kms = new KMS(pino);

	const result = await kms.decrypt(process.argv[2]);

	console.log(result.toString('utf8'));
})();