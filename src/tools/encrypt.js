'use strict';

const KMS = require('../lib/Kms');
const pino = require('pino')({ level: 'debug' });

(async () => {
	const kms = new KMS(pino);

	const result = await kms.encrypt(process.argv[2]);

	console.log(result.toString('base64'));
})();