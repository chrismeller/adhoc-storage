'use strict';

const config = require('config');
const pino = require('pino')({ level: 'debug' });
const level = require('level');
const uuidv4 = require('uuid/v4');
const fastify = require('fastify')();

const db = level(config.get('db_path'), {
	valueEncoding: 'json',
});

fastify.addContentTypeParser('*', (req, done) => {
	let data = '';

	req.on('data', chunk => {
		data = data + chunk;
	});

	req.on('end', () => {
		return done(null, data);
	});
});

fastify.post('/', async (req, res) => {
	try {
		const key = uuidv4();
		const secretKey = uuidv4();

		const value = {
			secretKey: secretKey,
			type: req.headers['content-type'],
			data: req.body,
		};

		await db.put(key, value);

		pino.info('Created new record.', { key: key });

		return res
			.header('X-Key', key)
			.header('X-Secret-Key', secretKey)
			.type(value.type)
			.send(value.data);
	}
	catch (e) {
		pino.error('Error while creating record!', { error: e });

		return res.code(500).send();
	}
});

fastify.get('/:key', async (req, res) => {
	try {
		pino.debug('Getting record.', { key: req.params.key });

		if (req.headers['x-secret-key'] === undefined) {
			return res.code(401).send();
		}

		const value = await db.get(req.params.key);

		if (req.headers['x-secret-key'] !== value.secretKey) {
			return res.code(401).send();
		}

		return res.type(value.type).send(value.data);
	}
	catch (e) {
		if (e.message.indexOf('Key not found in database') !== -1) {
			return res.code(404).send();
		}

		pino.error('Error while getting record!', e);

		return res.code(500).send();
	}
});

fastify.put('/:key', async (req, res) => {
	try {
		if (req.headers['x-secret-key'] === undefined) {
			return res.code(401).send();
		}

		const existing = await db.get(req.params.key);

		if (req.headers['x-secret-key'] !== existing.secretKey) {
			return res.code(401).send();
		}

		const value = {
			secretKey: existing.secretKey,
			type: req.headers['content-type'],
			data: req.body,
		};

		await db.put(req.params.key, value);

		res
			.header('X-Key', req.params.key)
			.header('X-Secret-Key', value.secretKey)
			.type(value.type)
			.send(value.data);
	}
	catch (e) {
		if (e.message.indexOf('Key not found in database') !== -1) {
			return res.code(404).send();
		}

		pino.error('Error while putting record!', e);

		return res.code(500).send();
	}
});

fastify.delete('/:key', async (req, res) => {
	try {
		if (req.headers['x-secret-key'] === undefined) {
			return res.code(401).send();
		}

		const existing = await db.get(req.params.key);

		if (req.headers['x-secret-key'] !== existing.secretKey) {
			return res.code(401).send();
		}

		await db.del(req.params.key);

		return res.code(200).send();
	}
	catch (e) {
		if (e.message.indexOf('Key not found in database') !== -1) {
			return res.code(404).send();
		}

		pino.error('Error while deleting record!', { error: e });

		return res.code(500).send();
	}
});

fastify.setErrorHandler((err, req, res) => {
	pino.error('Unhandled error!', { error: err });
});

(async () => {
	const port = process.env.PORT || 3000;

	await fastify.listen(port);

	pino.debug(`Listening on port ${port}`);
})();